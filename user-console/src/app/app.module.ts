import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BookComponent } from './book/book.component';
import { BookshelfComponent } from './bookshelf/bookshelf.component';
import { NewBookComponent } from './new-book/new-book.component';
import { PrintShelfComponent } from './print-shelf/print-shelf.component';

@NgModule({
  declarations: [
    AppComponent,
    BookComponent,
    BookshelfComponent,
    NewBookComponent,
    PrintShelfComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
