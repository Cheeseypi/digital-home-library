export interface Book {
  id: string;
  title: string;
  author: string;
  series?: string;
  series_number?: string;
  description: string;
  spine_url: string;
  cover_url: string;
  download_url: string;
}
