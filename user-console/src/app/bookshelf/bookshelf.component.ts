import {Component, Input, OnInit} from '@angular/core';
import {Book} from '../book.model';

@Component({
  selector: 'app-bookshelf',
  templateUrl: './bookshelf.component.html',
  styleUrls: ['./bookshelf.component.styl']
})
export class BookshelfComponent implements OnInit {
  @Input() Books: Book[];

  constructor() { }

  ngOnInit() {
  }

}
