import { Injectable } from '@angular/core';
import {Book} from './book.model';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  constructor() { }

  getBooks(): Book[] {
    return [
      {
        id: 'Test',
        title: 'A Great Book',
        author: 'Matt Doto',
        description: 'Lorem ipsum dolor sit amet',
        cover_url: 'https://upload.wikimedia.org/wikipedia/en/e/e0/Eldest_book_cover.png',
        spine_url: 'http://localhost:3000/static/spines/0a0bf011-7201-495a-8ad9-956fbb04398e_spine.png',
        download_url: 'https://upload.wikimedia.org/wikipedia/en/e/e0/Eldest_book_cover.png'
      },
      {
        id: 'Test',
        title: 'A Great Book',
        author: 'Matt Doto',
        description: 'Lorem ipsum dolor sit amet',
        cover_url: 'https://upload.wikimedia.org/wikipedia/en/e/e0/Eldest_book_cover.png',
        spine_url: 'http://localhost:3000/static/spines/ebceacdb-deda-4d79-bb45-cc5d36ec059a_spine.png',
        download_url: 'https://upload.wikimedia.org/wikipedia/en/e/e0/Eldest_book_cover.png'
      },
      {
        id: 'Test',
        title: 'A Great Book',
        author: 'Matt Doto',
        description: 'Lorem ipsum dolor sit amet',
        cover_url: 'https://upload.wikimedia.org/wikipedia/en/e/e0/Eldest_book_cover.png',
        spine_url: 'http://localhost:3000/static/spines/967bb7fb-4c3a-4b82-bfb8-c14f956573f1_spine.png',
        download_url: 'https://upload.wikimedia.org/wikipedia/en/e/e0/Eldest_book_cover.png'
      },
    ];
  }

  getBook(bookId: string): Book {
    return {
      id: 'Test',
      title: 'A Great Book',
      author: 'Matt Doto',
      description: 'Lorem ipsum dolor sit amet',
      cover_url: 'https://upload.wikimedia.org/wikipedia/en/e/e0/Eldest_book_cover.png',
      spine_url: 'https://images.gr-assets.com/books/1328325653l/13076132.jpg',
      download_url: 'https://upload.wikimedia.org/wikipedia/en/e/e0/Eldest_book_cover.png'
    };
  }
}
