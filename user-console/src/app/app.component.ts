import { Component } from '@angular/core';
import {Book} from './book.model';
import {BookService} from './book.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.styl']
})
export class AppComponent {
  title = 'user-console';

  Books: Book[] = [];

  constructor(private bookService: BookService){}

  ngOnInit(){
    this.Books = this.bookService.getBooks();
  }
}
