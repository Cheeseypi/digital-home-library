import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintShelfComponent } from './print-shelf.component';

describe('PrintShelfComponent', () => {
  let component: PrintShelfComponent;
  let fixture: ComponentFixture<PrintShelfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintShelfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintShelfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
